package com.gmail.nikcody92.android.rulerfactory.resizablegauge;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.nikcody92.android.rulerfactory.resizablegauge.model.TransformRect;
import com.gmail.nikcody92.android.rulerfactory.resizablegauge.tweaks.ViewStandardMatrix;
import com.gmail.nikcody92.android.rulerfactory.touchdispatcher.observer.TouchObserver;


/**
 * Resizable view to select and eventually measure objects on the screen.
 * It is meant to be used inside a FrameLayout or a RelativeLayout.
 */
public class ResizableGauge extends View implements TouchObserver {
    protected Drawable pattern;
    private int baseWidth, baseHeight;
    private int layoutWidth, layoutHeight;
    protected boolean active;

    @Override
    public boolean isTouched() {
        return active;
    }

    @Override
    public void handleDown(boolean b, int x, int y) {
        active = false;
        if (b)
            return;
        TransformRect area = getShapeArea();
        float corner1[] = {area.coords[0], area.coords[1]},
                corner2[] = {area.coords[0] + area.getWidth(), area.coords[1] + area.getHeight()};
        RectF horizontalArea = new RectF(corner1[0], corner1[1], corner2[0], corner2[1]);
        PointF touchPoint = new PointF(x, y);
        touchPoint = area.antiRotate(touchPoint);
        active = horizontalArea.contains(touchPoint.x, touchPoint.y);
    }

    @Override
    public void doScroll(int deltaX, int deltaY) {
        if (!active)
            return;
        animate().translationXBy(deltaX)
                .translationYBy(deltaY)
                .setDuration(0);
    }

    @Override
    public void doLongPress(int i, int i1) {

    }

    @Override
    public void doFling(float v, float v1) {

    }

    @Override
    public void doScale(float v) {

    }

    /**
     * Object updating base size of the gauge according to layout bounds.
     */
    private class BaseSizeChange implements OnLayoutChangeListener {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                   int oldLeft, int oldTop, int oldRight, int oldBottom) {
            baseWidth = right - left;
            baseHeight = bottom - top;
            setShapeWidth(layoutWidth);
            setShapeHeight(layoutHeight);
        }
    }

    /**
     * Force a fixed size for layout-defined view, in order to make resizing predictable elsewhere.
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int defaultWidth, defaultHeight;
        int measuredWidth, measuredHeight;
        defaultWidth = getResources().getDimensionPixelSize(R.dimen.shape_base_width);
        defaultHeight = getResources().getDimensionPixelSize(R.dimen.shape_base_height);
        switch (MeasureSpec.getMode(widthMeasureSpec)) {
            case MeasureSpec.UNSPECIFIED:
                measuredWidth = defaultWidth;
                break;
            case MeasureSpec.AT_MOST:
                measuredWidth = Math.min(MeasureSpec.getSize(widthMeasureSpec), defaultWidth);
                break;
            case MeasureSpec.EXACTLY:
                measuredWidth = MeasureSpec.getSize(widthMeasureSpec);
                break;
            default:
                measuredWidth = 0;
        }
        switch (MeasureSpec.getMode(heightMeasureSpec)) {
            case MeasureSpec.UNSPECIFIED:
                measuredHeight = defaultHeight;
                break;
            case MeasureSpec.AT_MOST:
                measuredHeight = Math.min(MeasureSpec.getSize(heightMeasureSpec), defaultHeight);
                break;
            case MeasureSpec.EXACTLY:
                measuredHeight = MeasureSpec.getSize(heightMeasureSpec);
                break;
            default:
                measuredHeight = 0;
        }
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    /**
     * Initialization stage for this view common to all constructors.
     */
    void initGauge() {
        pattern = null;
        baseWidth = getResources().getDimensionPixelSize(R.dimen.shape_base_width);
        baseHeight = getResources().getDimensionPixelSize(R.dimen.shape_base_height);
        layoutWidth = baseWidth;
        layoutHeight = baseHeight;
        active = false;
    }

    public ResizableGauge(Context context) {
        super(context);
        initGauge();
    }

    public ResizableGauge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ResizableGauge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGauge();
        TypedArray attrArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ResizableGauge, defStyleAttr, R.style.defaultGaugeStyle);
        try {
            this.pattern = attrArray.getDrawable(R.styleable.ResizableGauge_shape);
            this.layoutWidth = attrArray.getDimensionPixelSize(R.styleable.ResizableGauge_shape_width, this.baseWidth);
            this.layoutHeight = attrArray.getDimensionPixelSize(R.styleable.ResizableGauge_shape_height, this.baseHeight);
        } finally {
            attrArray.recycle();
        }
        addOnLayoutChangeListener(new BaseSizeChange());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        pattern.setBounds(0, 0, getWidth() - 1, getHeight() - 1);
        pattern.draw(canvas);
    }


    /**
     * Retrieve the effective width of the gauge applying all transformations on it.
     * @return The gauge's width in pixels.
     */
    public int getShapeWidth() {
        return Math.round(getScaleX() * baseWidth);
    }

    /**
     * Retrieve the effective height of the gauge applying all transformations on it.
     * @return The gauge's height in pixels.
     */
    public int getShapeHeight() {
        return Math.round(getScaleY() * baseHeight);
    }

    /**
     * Set view's width, keeping its current position.
     *
     * @param newWidth  Requested width.
     */
    public void setShapeWidth(int newWidth) throws IllegalArgumentException {
        TransformRect oldArea = getShapeArea(), newArea;
        PointF areaStart = new PointF(oldArea.coords[0], oldArea.coords[1]);
        if (newWidth < 0)
            throw new IllegalArgumentException("Invalid width for the gauge.");
        else if (newWidth == 0) {
            ViewGroup.LayoutParams dimParams = getLayoutParams();
            dimParams.width = newWidth;
            setLayoutParams(dimParams);
            return;
        }
        newArea = TransformRect.createFromTransform(areaStart, newWidth, oldArea.getHeight(),
                oldArea.getRotation());
        setShapeArea(newArea);
    }

    /**
     * Set view's height, keeping its current position.
     *
     * @param newHeight  Requested height.
     */
    public void setShapeHeight(int newHeight) throws IllegalArgumentException {
        TransformRect oldArea = getShapeArea(), newArea;
        PointF areaStart = new PointF(oldArea.coords[0], oldArea.coords[1]);
        if (newHeight < 0)
            throw new IllegalArgumentException("Invalid height for the gauge.");
        if (newHeight == 0) {
            ViewGroup.LayoutParams dimParams = getLayoutParams();
            dimParams.height = newHeight;
            setLayoutParams(dimParams);
            return;
        }
        newArea = TransformRect.createFromTransform(areaStart, oldArea.getWidth(), newHeight,
                oldArea.getRotation());
        setShapeArea(newArea);
    }

    /**
     * Retrieve a representation of the area occupied by the view.
     *
     * @return A rectangle which contains the current view.
     */
    public TransformRect getShapeArea() {
        float shapeStart[] = {0, 0};
        getMatrix().mapPoints(shapeStart);
        return TransformRect.createFromTransform(new PointF(shapeStart[0], shapeStart[1]), getShapeWidth(), getShapeHeight(),
                getRotation());
    }

    /**
     * Move and resize the view according to the given area.
     *
     * @param newArea Rectangle representing the area meant to contain the view.
     */
    public void setShapeArea(TransformRect newArea) {
        float newScaleX, newScaleY, newRotation;
        Matrix shapeTransform;
        float newDrift[] = {0, 0};
        newScaleX = newArea.getWidth() / baseWidth;
        newScaleY = newArea.getHeight() / baseHeight;
        newRotation = newArea.getRotation();
        shapeTransform = ViewStandardMatrix.create(this, newScaleX, newScaleY, newRotation);
        shapeTransform.mapPoints(newDrift);
        animate().scaleX(newScaleX)
                .scaleY(newScaleY)
                .rotation(newRotation)
                .translationX(newArea.coords[0] - newDrift[0])
                .translationY(newArea.coords[1] - newDrift[1])
                .setDuration(0);
    }

    /**
     * Set the shape for the gauge and update its contents.
     *
     * @param drawable A drawable for the shape.
     */
    public void setShape(Drawable drawable) {
        pattern = drawable;
        invalidate();
    }

}
