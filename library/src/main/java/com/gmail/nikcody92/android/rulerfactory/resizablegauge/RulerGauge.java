package com.gmail.nikcody92.android.rulerfactory.resizablegauge;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;

import com.gmail.nikcody92.android.rulerfactory.resizablegauge.model.RulerShape;
import com.gmail.nikcody92.android.rulerfactory.resizablegauge.model.TransformRect;

public class RulerGauge extends ResizableGauge {
    private RulerShape shape;
    private boolean activeA, activeB;
    private float minWidth,  ratio;

    @Override
    public boolean isTouched() {
        return active || activeA || activeB;
    }

    @Override
    public void doScroll(int deltaX, int deltaY) {
        if (activeA)
            doScrollA(deltaX, deltaY);
        else if (activeB)
            doScrollB(deltaX, deltaY);
        else if (active)
            super.doScroll(deltaX, deltaY);
    }

    public void initRuler() {
        shape = new RulerShape();
        pattern = new ShapeDrawable(shape);
        Paint paint = ((ShapeDrawable) pattern).getPaint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        activeA = false;
        activeB = false;
        minWidth = 8;
        ratio = 0.125f;
    }

    public RulerGauge(Context context) {
        super(context);
        initRuler();
    }

    public RulerGauge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RulerGauge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initRuler();
        TypedArray attrArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RulerGauge, defStyleAttr, R.style.defaultRulerStyle);
        int thickness, color;
        try {
            thickness = attrArray.getDimensionPixelSize(R.styleable.RulerGauge_thickness, 2);
            color = attrArray.getColor(R.styleable.RulerGauge_color,
                    ContextCompat.getColor(getContext(), android.R.color.black));
            this.minWidth = attrArray.getDimension(R.styleable.RulerGauge_min_width, minWidth);
            this.ratio = attrArray.getFraction(R.styleable.RulerGauge_ratio, 1, 1, ratio);
        } finally {
            attrArray.recycle();
        }
        setThickness(thickness);
        setColor(color);
    }

    public void setThickness(int thickness) {
        Paint paint = ((ShapeDrawable) pattern).getPaint();
        paint.setStrokeWidth(thickness);
        invalidate();
    }

    public void setColor(@ColorInt int color) {
        Paint paint = ((ShapeDrawable) pattern).getPaint();
        paint.setColor(color);
        invalidate();
    }

    public PointF getPointA() {
        TransformRect area = getShapeArea();
        PointF pointA = new PointF();
        pointA.set(area.coords[0], area.coords[1] + area.getHeight() * 0.5f);
        return area.rotate(pointA);
    }

    public PointF getPointB() {
        TransformRect area = getShapeArea();
        PointF pointB = new PointF();
        pointB.set(area.coords[0] + area.getWidth(), area.coords[1] + area.getHeight() * 0.5f);
        return area.rotate(pointB);
    }

    private float getDistance(PointF pointA, PointF pointB) {
        float deltaX, deltaY;
        deltaX = pointB.x - pointA.x;
        deltaY = pointB.y - pointA.y;
        return (float) Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    private float getAdaptedHeight (float width) {
        return width * ratio;
    }

    private void setLine(PointF pointA, PointF pointB) {
        float deltaY, distance, newRotation, height;
        float pointDrift[] = new float[2];
        PointF corner = new PointF();
        Matrix rotationTransform = new Matrix();
        TransformRect newArea;
        distance = getDistance(pointA, pointB);
        distance = distance >= minWidth ? distance : minWidth;
        height = getAdaptedHeight(distance);
        pointDrift[0] = 0;
        pointDrift[1] = height * 0.5f;
        deltaY = pointB.y - pointA.y;
        newRotation = (float) Math.toDegrees(Math.asin(deltaY / distance));
        if (pointA.x > pointB.x)
            newRotation = 180 - newRotation;
        rotationTransform.postRotate(newRotation, 0, 0);
        rotationTransform.mapPoints(pointDrift);
        corner.set(pointA.x - pointDrift[0], pointA.y - pointDrift[1]);
        newArea = TransformRect.createFromTransform(corner, distance, height, newRotation);
        setShapeArea(newArea);
    }

    public void doScrollA(float deltaX, float deltaY) {
        PointF pointA = getPointA(), pointB = getPointB();
        pointA.set(pointA.x + deltaX, pointA.y + deltaY);
        setLine(pointA, pointB);
    }

    public void doScrollB(float deltaX, float deltaY) {
        PointF pointA = getPointA(), pointB = getPointB();
        pointB.set(pointB.x + deltaX, pointB.y + deltaY);
        setLine(pointA, pointB);
    }

    @Override
    public void handleDown(boolean b, int x, int y) {
        active = activeA = activeB = false;
        if (b)
            return;
        TransformRect area = getShapeArea();
        PointF pointA = getPointA(), pointB = getPointB(), touchPoint = new PointF(x, y);
        if (TransformRect.getDistance(pointA, touchPoint) < 0.75f * getShapeHeight())
            activeA = true;
        else if (TransformRect.getDistance(pointB, touchPoint) < 0.75f * getShapeHeight())
            activeB = true;
        if (activeA || activeB)
            return;
        float corner1[] = {area.coords[0], area.coords[1]},
                corner2[] = {area.coords[0] + area.getWidth(), area.coords[1] + area.getHeight()};
        RectF horizontalArea = new RectF(corner1[0], corner1[1], corner2[0], corner2[1]);
        Matrix rectScaling = new Matrix();
        rectScaling.postScale(1.5f, 1.5f, horizontalArea.centerX(), horizontalArea.centerY());
        rectScaling.mapPoints(corner1);
        rectScaling.mapPoints(corner2);
        horizontalArea = new RectF(corner1[0], corner1[1], corner2[0], corner2[1]);
        touchPoint = area.antiRotate(touchPoint);
        active = horizontalArea.contains(touchPoint.x, touchPoint.y);
    }
}
