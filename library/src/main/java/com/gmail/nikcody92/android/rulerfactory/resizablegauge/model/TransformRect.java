package com.gmail.nikcody92.android.rulerfactory.resizablegauge.model;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Representation for a rectangle which may be rotated.
 * Actually there is not any control to make sure it is a rectangle.
 */
public class TransformRect implements Parcelable {
    public float coords[];      // sequence of coordinates for the upper-left, upper-right, bottom-left, bottom-right corners respectively

    public TransformRect(float... points) throws IllegalArgumentException {
        if (points.length != 8)
            throw new IllegalArgumentException("Number of arguments is not right");
        this.coords = points;
    }

    public TransformRect(RectF rect) {
        this(rect.left, rect.top, rect.right, rect.top, rect.left, rect.bottom, rect.right, rect.bottom);
    }

    public static Parcelable.Creator<TransformRect> CREATOR = new Parcelable.Creator<TransformRect>() {
        @Override
        public TransformRect createFromParcel(Parcel source) {
            float points[] = new float[8];
            source.readFloatArray(points);
            return new TransformRect(points);
        }

        @Override
        public TransformRect[] newArray(int size) {
            return new TransformRect[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloatArray(coords);
    }

    /**
     * Create rectangle from coordinates of the upper-left corner and its characteristics.
     *
     * @param start    Array with coordinates of the upper-left corner.
     * @param width    Size for the first side defining the rectangle.
     * @param height   Size for the second side defining the rectangle.
     * @param rotation Clockwise rotation around the horizontal positive axis.
     * @return A newly allocated rectangle.
     */
    public static TransformRect createFromTransform(PointF start, float width, float height, float rotation) {
        float horizontalDeltaX, horizontalDeltaY, verticalDeltaX, verticalDeltaY;
        horizontalDeltaX = (float) (width * Math.cos(Math.toRadians(rotation)));
        horizontalDeltaY = (float) (width * Math.sin(Math.toRadians(rotation)));
        verticalDeltaX = (float) (-height * Math.sin(Math.toRadians(rotation)));
        verticalDeltaY = (float) (height * Math.cos(Math.toRadians(rotation)));
        return new TransformRect(start.x, start.y,
                start.x + horizontalDeltaX, start.y + horizontalDeltaY,
                start.x + verticalDeltaX, start.y + verticalDeltaY,
                start.x + horizontalDeltaX + verticalDeltaX,
                start.y + horizontalDeltaY + verticalDeltaY);
    }

    public static float getDistance(PointF pointA, PointF pointB) {
        float deltaX, deltaY;
        deltaX = pointB.x - pointA.x;
        deltaY = pointB.y - pointA.y;
        return (float) Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public float getWidth() {
        PointF upperCorner1 = new PointF(), upperCorner2 = new PointF();
        upperCorner1.set(coords[0], coords[1]);
        upperCorner2.set(coords[2], coords[3]);
        return getDistance(upperCorner1, upperCorner2);
    }

    public float getHeight() {
        PointF leftCorner1 = new PointF(), leftCorner2 = new PointF();
        leftCorner1.set(coords[0], coords[1]);
        leftCorner2.set(coords[4], coords[5]);
        return getDistance(leftCorner1, leftCorner2);
    }

    public float getRotation() {
        PointF upperCorner1 = new PointF(), upperCorner2 = new PointF();
        float deltaY, rotation;
        upperCorner1.set(coords[0], coords[1]);
        upperCorner2.set(coords[2], coords[3]);
        deltaY = upperCorner2.y - upperCorner1.y;
        rotation = (float) Math.toDegrees(Math.asin(deltaY / getDistance(upperCorner1, upperCorner2)));
        if (upperCorner1.x > upperCorner2.x)
            rotation = 180 - rotation;
        return rotation;
    }

    /**
     * Apply a transformation to the given rectangle, by transforming each one of its corners.
     *
     * @param origin    Source rectangle.
     * @param transform Matrix representing the transformation.
     * @return Resulting rectangle in a newly allocated object.
     */
    public static TransformRect applyTransform(TransformRect origin, Matrix transform) {
        float transformPoints[] = new float[8];
        transform.mapPoints(transformPoints, origin.coords);
        return new TransformRect(transformPoints);
    }

    public static RectF toRect (TransformRect tRect) {
        RectF rect = new RectF();
        rect.left = tRect.coords[0];
        rect.top = tRect.coords[1];
        rect.right = rect.left + tRect.getWidth();
        rect.bottom = rect.top + tRect.getHeight();
        return rect;
    }

    public PointF rotate(PointF pnt) {
        float origin[] = {pnt.x, pnt.y}, result[] = new float[2];
        Matrix rotationTransform = new Matrix();
        rotationTransform.postRotate(getRotation(), coords[0], coords[1]);
        rotationTransform.mapPoints(result, origin);
        return new PointF(result[0], result[1]);
    }

    public PointF antiRotate(PointF pnt) {
        float origin[] = {pnt.x, pnt.y}, result[] = new float[2];
        Matrix rotationTransform = new Matrix();
        rotationTransform.postRotate(-getRotation(), coords[0], coords[1]);
        rotationTransform.mapPoints(result, origin);
        return new PointF(result[0], result[1]);
    }

}
