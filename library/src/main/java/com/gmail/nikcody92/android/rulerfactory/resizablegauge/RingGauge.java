package com.gmail.nikcody92.android.rulerfactory.resizablegauge;


import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;

import com.gmail.nikcody92.android.rulerfactory.resizablegauge.model.TransformRect;

public class RingGauge extends ResizableGauge {

    public RingGauge(Context context) {
        super(context);
        pattern = getResources().getDrawable(R.drawable.ring_shape);
    }

    public RingGauge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RingGauge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        pattern = getResources().getDrawable(R.drawable.ring_shape);
    }

    @Override
    public void handleDown(boolean touched, int x, int y) {
        active = false;
        if (touched)
            return;
        TransformRect area = getShapeArea();
        PointF center = new PointF(), touchPoint = new PointF(x, y);
        center.set(area.coords[0] + area.getWidth() * 0.5f, area.coords[1] + area.getHeight() * 0.5f);
        active = TransformRect.getDistance(center, touchPoint) < 0.75f * area.getWidth();
    }
}
