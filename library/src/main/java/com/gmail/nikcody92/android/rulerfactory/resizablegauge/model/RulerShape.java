package com.gmail.nikcody92.android.rulerfactory.resizablegauge.model;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.shapes.Shape;


/**
 * Shape representing a ruler.
 * A new drawing path is created every time the shape is resized.
 */
public class RulerShape extends Shape {
    private Path path;

    private Path createPath() {
        Path newPath = new Path();
        newPath.lineTo(0, getHeight() - 1);
        newPath.moveTo(0, getHeight() * 0.5f - 1);
        newPath.lineTo(getWidth() - 1, getHeight() * 0.5f - 1);
        newPath.moveTo(getWidth() - 1, 0);
        newPath.lineTo(getWidth() - 1, getHeight() - 1);
        return newPath;
    }

    public RulerShape() {
        super();
        path = null;
    }

    @Override
    protected void onResize(float width, float height) {
        super.onResize(width, height);
        path = createPath();
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        if (path == null)
            return;
        canvas.drawPath(path, paint);

    }
}
