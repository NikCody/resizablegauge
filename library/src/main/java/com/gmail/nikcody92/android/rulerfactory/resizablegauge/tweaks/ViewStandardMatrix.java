package com.gmail.nikcody92.android.rulerfactory.resizablegauge.tweaks;

import android.graphics.Matrix;
import android.view.View;


/**
 * Utility class to obtain a preview of the standard behaviour regarding scaling and rotation in a view.
 */
public final class ViewStandardMatrix {

    public static Matrix create(View view) {
        return create(view, view.getScaleX(), view.getScaleY(), view.getRotation());
    }

    public static Matrix create(View view, float scaleX, float scaleY, float rotation) {
        Matrix matrix = new Matrix();
        float pivotX, pivotY;
        pivotX = view.getPivotX();
        pivotY = view.getPivotY();
        matrix.postScale(scaleX, scaleY, pivotX, pivotY);
        matrix.postRotate(rotation, pivotX, pivotY);
        return matrix;
    }
}
